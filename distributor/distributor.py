import json
import asyncio
import uuid
from time import time
from flask import request


from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN

#NATS
NATS_AUTH_TOKEN = "defaultFissionAuthToken"
NATS_SVC_URL = "nats-streaming.fission.svc.cluster.local"
NATS_PORT = 4222

NATS_URL = "nats://{}@{}:{}".format(NATS_AUTH_TOKEN, NATS_SVC_URL, NATS_PORT)

def main():
    chunk, keywords, query_id, timeout = json.loads(request.data.decode('utf-8'))

    async def distribute(loop):
        _nc = NATS()
        await _nc.connect(servers=[NATS_URL], loop=loop)
        _sc = STAN()
        await _sc.connect("fissionMQTrigger", "distributor{}".format(str(uuid.uuid4())), nats=_nc)
        #don't wait for acknowledgment
        async def ack_handler(_):
            pass
        for url in chunk:
            if timeout < time():
                break
            await _sc.publish("crawling", \
                    json.dumps((url, keywords, query_id, timeout)).encode(), \
                    ack_handler=ack_handler)
        await asyncio.sleep(1, loop=loop)
        await _sc.close()
        await _nc.close()
    loop = asyncio.new_event_loop()
    loop.run_until_complete(distribute(loop))
    loop.close()
    return ""
