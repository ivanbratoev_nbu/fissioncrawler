#!/usr/bin/python3
from mimetypes import MimeTypes
import boto3
import os


mime = MimeTypes()

session = boto3.session.Session()
client = session.client('s3',
                        region_name='ams3',
                        endpoint_url='https://ams3.digitaloceanspaces.com',
                        aws_access_key_id=os.environ['DO_SPACE_KEY'],
                        aws_secret_access_key=os.environ['DO_SPACE_SECRET'])


for entry in os.scandir('static'):
    file = entry.name
    mime_type, _ = mime.guess_type(file)
    client.upload_file(os.path.join('static', file), \
    'fissioncrawler', file, ExtraArgs={'ACL': 'public-read', 'ContentType': mime_type})
