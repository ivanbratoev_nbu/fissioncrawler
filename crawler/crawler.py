import json
from urllib.parse import urljoin, urldefrag
from time import time
import asyncio
from functools import reduce
import operator
from flask import request
from redis import Redis
import aiohttp

REDIS_HOST = "sullen-buffalo-redis-master.default.svc.cluster.local"
REDIS_PORT = 6379
REDIS_PASSWORD = "HvUN6s3RTK"


async def get_body(client, url, keywords, redis, query_id):
    async with client.get(url) as response:
        data = await response.read()
        lower_data = data.decode('utf-8').lower()
        count = reduce(operator.add,[lower_data.count(w.lower()) for w in keywords])
        redis.zadd(query_id, {url: count})

def remove_fragment(url):
    pure_url, frag = urldefrag(url)
    return pure_url

def get_links(html):
    new_urls = [link.split('"')[0] for link in str(html).replace("'",'"').split('href="')[1:]]
    return [urljoin(root_url, remove_fragment(new_url)) for new_url in new_urls]

def main():
    url, keywords, query_id, timeout = json.loads(request.data.decode('utf-8'))

    redis = Redis(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASSWORD)

    crawled_urls = []
    url_hub = [url, ]

    loop = asyncio.get_event_loop()
    client = aiohttp.ClientSession(loop=loop)
    for to_crawl in url_hub:
        if time() >= timeout:
            break
        raw_html = loop.run_until_complete(get_body(client, to_crawl, keywords, redis, query_id))
        for link in get_links(raw_html):
            if root_url in link and not link in crawled_urls:
                url_hub.append(link)
        url_hub.remove(to_crawl)
        crawled_urls.append(to_crawl)
        print("url hub: {} | crawled: {}  |url : {}".format(len(url_hub), len(crawled_urls), to_crawl))
    client.close()
    return ""
