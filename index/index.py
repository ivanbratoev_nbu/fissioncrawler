import os
from time import time, sleep
from threading import Thread
import json
import urllib
import asyncio
import uuid
from flask import render_template, request
import pymongo
from redis import Redis
#import operator
from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN

STATIC_URL = 'https://fissioncrawler.ams3.digitaloceanspaces.com'
TIMEOUT = 5.0
RESULTS_NUM = 20
#MONGO_CONN_STRING = "mongodb://crawler:crawler@domains-shard-00-00-bncxy.mongodb.net:27017,domains-shard-00-01-bncxy.mongodb.net:27017,domains-shard-00-02-bncxy.mongodb.net:27017/test?ssl=true&replicaSet=domains-shard-0&authSource=admin&retryWrites=true"  #pylint: disable=line-too-long
MONGO_CONN_STRING = "mongodb://139.59.209.186:27017"
__CHUNK_SIZE = 5

#NATS
NATS_AUTH_TOKEN = "defaultFissionAuthToken"
NATS_SVC_URL = "nats-streaming.fission.svc.cluster.local"
NATS_PORT = 4222

NATS_URL = "nats://{}@{}:{}".format(NATS_AUTH_TOKEN, NATS_SVC_URL, NATS_PORT)

REDIS_HOST = "sullen-buffalo-redis-master.default.svc.cluster.local"
REDIS_PORT = 6379
REDIS_PASSWORD = "HvUN6s3RTK"

# I can't seem to manage to deploy the templates, so downloading from
# statics it is.
def copy_templates():
    os.makedirs('templates', exist_ok=True)
    urllib.request.urlretrieve('{}/index.html'.format(STATIC_URL), 'templates/index.html')

# ugliest hack in the world, but works. TODO
copy_templates()



# [ urls ]
def __get_domain_chunks(domain):
    client = pymongo.MongoClient(MONGO_CONN_STRING)
    _db = client.domains
    collection = _db.domains
    if domain:
        cursor = collection.find({'domain': domain})
    else:
        cursor = collection.find()
    cursor.batch_size(__CHUNK_SIZE)
    chunk = []
    for item in cursor:
        if len(chunk) >= __CHUNK_SIZE:
            yield chunk
            chunk = []
        chunk.append(item['url'])

def distribute_request(domain, keywords, query_id, timeout):
    async def distribute(loop):
        _nc = NATS()
        await _nc.connect(servers=[NATS_URL], loop=loop)
        _sc = STAN()
        await _sc.connect("fissionMQTrigger", "index{}".format(str(uuid.uuid4())), nats=_nc)
        #don't wait for acknowledgment
        async def ack_handler(_):
            pass
        for chunk in __get_domain_chunks(domain):
            if time() >= timeout:
                break
            await _sc.publish("distribution", \
                    json.dumps((chunk, keywords, query_id, timeout)).encode(), \
                    ack_handler=ack_handler)
        await asyncio.sleep(1, loop=loop)
        await _sc.close()
        await _nc.close()
    loop = asyncio.new_event_loop()
    loop.run_until_complete(distribute(loop))
    loop.close()

def main():
    keywords = request.args.get('keywords', None)
    if keywords:
        keywords = [s for s in keywords.split(' ') if s.strip()]
        domain = request.args.get('domain', None)
        timeout = time() + TIMEOUT

        query_id = str(uuid.uuid4())
        redis = Redis(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASSWORD)

        Thread(target=distribute_request, \
                args=(domain, keywords, query_id, timeout)).start()
        while timeout > time():
            sleep(0.5)
            #if redis.zcount(query_id, '-inf', '+inf') >= RESULTS_NUM:
            #    break
        res = redis.zrangebyscore(query_id, '-inf', '+inf')
        redis.expire(query_id, 1)
        res.reverse()

        return render_template('index.html', static_url=STATIC_URL, results=res)
    else:
        return render_template('index.html', static_url=STATIC_URL)
